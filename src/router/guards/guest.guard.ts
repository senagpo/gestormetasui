import { NavigationGuardNext, Route } from 'vue-router';

import { AxiosConfigured as Axios } from '@/components/tools/axios';

export default async function guestGuard(to: Route, from: Route, next: NavigationGuardNext): Promise<void> {
	try {
		const { data } = await Axios.get('api/isguest');
		const guardState = data.valid || false;
		if (guardState) next();
		else next({ name: 'Home' });
		console.info(`Executed Guard [GUEST] with state ${guardState}`);
	} catch (error) {
		next({ name: 'Home' });
		console.error(`Catched error or invalid at guard [GUEST] ${error}`);
	}
}
