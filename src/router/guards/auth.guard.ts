import store from '@/store';
import _ from 'lodash';
import { wipeCookies } from '@/components/tools/util/Functions';
import { NavigationGuardNext, Route } from 'vue-router';
import { AxiosConfigured as Axios } from '@/components/tools/axios';

export default async function authGuard(to: Route, from: Route, next: NavigationGuardNext): Promise<void> {
	try {
		const { data } = await Axios.get('api/isauth');
		const guardState = data.valid || false;
		if (guardState) {
			if (!store.state.authState || _.isEmpty(store.state.authUser) || _.isEmpty(store.state.authRole)) await store.dispatch('syncUser');
			next();
		} else {
			wipeCookies();
			store.commit('wipeAuth');
			next({ name: 'Login' });
		}
		console.info(`Executed Guard [AUTH] with state ${guardState}`);
	} catch (error) {
		wipeCookies();
		store.commit('wipeAuth');
		next({ name: 'Login' });
		console.error(`Catched error or invalid at guard [AUTH] ${error}`);
	}
}
