import { AxiosConfigured as Axios } from '@/components/tools/axios';
import { NavigationGuardNext, Route } from 'vue-router';

export default async function managerGuard(to: Route, from: Route, next: NavigationGuardNext): Promise<void> {
	try {
		const { data } = await Axios.get('api/ismanager');
		const guardState = data.valid || false;
		if (guardState) {
			next();
		} else next({ name: 'Home' });
		console.info(`Executed Guard [MANAGER] with state ${guardState}`);
	} catch (error) {
		next({ name: 'Home' });
		console.error(`Catched error or invalid at guard [MANAGER] ${error}`);
	}
}
