import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import authGuard from '@/router/guards/auth.guard';
import guestGuard from '@/router/guards/guest.guard';
import adminGuard from '@/router/guards/admin.guard';
import managerGuard from '@/router/guards/manager.guard';
import regionalGuard from '@/router/guards/regional.guard';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
	{
		path: '/',
		name: 'Login',
		component: () => import(/* webpackChunkName: "login" */ '@/views/auth/Login.vue'),
		beforeEnter: async (to, from, next) => {
			await guestGuard(to, from, next);
		},
	},
	{
		path: '/home',
		name: 'Home',
		component: () => import(/* webpackChunkName: "home" */ '@/views/common/Home.vue'),
		beforeEnter: async (to, from, next) => {
			await authGuard(to, from, next);
		},
	},
	{
		path: '/user/password',
		name: 'ChangePassword',
		component: () => import(/* webpackChunkName: "changepassword" */ '@/views/auth/ChangePassword.vue'),
		beforeEnter: async (to, from, next) => {
			await authGuard(to, from, next);
		},
	},
	{
		path: '/goals/:target(populations|schools|formation)/upload',
		name: 'UploadGoals',
		component: () => import(/* webpackChunkName: "uploadgoals" */ '@/views/goals/UploadGoals.vue'),
		beforeEnter: async (to, from, next) => {
			await authGuard(to, from, next);
			await adminGuard(to, from, next);
			await managerGuard(to, from, next);
		},
	},
	{
		path: '/goals/disaggregate/populations',
		name: 'PopulationGoals',
		component: () => import(/* webpackChunkName: "PopulationGoalsDisaggregation" */ '@/views/goals/VulnerablePopulations/Disaggregation.vue'),
		beforeEnter: async (to, from, next) => {
			await authGuard(to, from, next);
			await regionalGuard(to, from, next);
		},
	},
	{
		path: '/goals/disaggregate/technical',
		name: 'TechnicalGoals',
		component: () => import(/* webpackChunkName: "TechnicalSchoolsDisaggregation" */ '@/views/goals/TechnicalSchools/Disaggregation.vue'),
		beforeEnter: async (to, from, next) => {
			await authGuard(to, from, next);
			await regionalGuard(to, from, next);
		},
	},
	{
		path: '/goals/disaggregate/formation',
		name: 'FormationGoals',
		component: () => import(/* webpackChunkName: "FormationDisaggregation" */ '@/views/goals/Formation/Disaggregation.vue'),
		beforeEnter: async (to, from, next) => {
			await authGuard(to, from, next);
			await regionalGuard(to, from, next);
		},
	},
	{
		path: '/exports/generate',
		name: 'Exports',
		component: () => import(/* webpackChunkName: "Exports" */ '@/views/common/Export.vue'),
		beforeEnter: async (to, from, next) => {
			await authGuard(to, from, next);
			await adminGuard(to, from, next);
			await managerGuard(to, from, next);
		},
	},
	{
		path: '/budget/assign',
		name: 'Budget',
		component: () => import(/* webpackChunkName: "Budgets" */ '@/views/budget/Assign.vue'),
		beforeEnter: async (to, from, next) => {
			await authGuard(to, from, next);
			await regionalGuard(to, from, next);
		},
	},
	{
		path: '/validity/management',
		name: 'Validities',
		component: () => import(/* webpackChunkName: "ValidityManagement" */ '@/views/validity/Management.vue'),
		beforeEnter: async (to, from, next) => {
			await authGuard(to, from, next);
			await adminGuard(to, from, next);
		},
	},
];

const router = new VueRouter({
	routes,
});

export default router;
