import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import Swal from 'sweetalert2';
import { AxiosError } from 'axios';
import Axios from '@/components/tools/axios';
import { wipeCookies } from '@/components/tools/util/Functions';

export default new Vuex.Store({
	state: {
		isLoading: false,
		authState: false,
		authUser: {} as { id: number; name: string; email: string } | object,
		authRole: {} as { id: number; name: string; display: string } | object,
		authRegional: {} as { id: number; name: string } | object,
		validity: {} as object,
	},
	mutations: {
		setAuthState: function(state, newstate: boolean) {
			state.authState = newstate;
		},
		setAuthUser: function(state, { user, role, regional }) {
			state.authUser = user;
			state.authRole = role;
			state.authRegional = regional;
		},
		setValidity: function(state, { validity }) {
			state.validity = validity;
		},
		setLoading: function(state, load: boolean) {
			state.isLoading = load;
		},
		wipeAuth: function(state) {
			state.authState = false;
			state.authUser = {};
			state.authRole = {};
			state.authRegional = {};
		},
	},
	actions: {
		signIn: async function({ commit, dispatch }, formData: object) {
			try {
				wipeCookies();

				await Axios.get('sanctum/csrf-cookie');
				const { data } = await Axios.post('api/login', formData);
				if (data.success) {
					commit('setAuthUser', data);
					await dispatch('syncUser');
					commit('setAuthState', true);
				}
			} catch (error) {
				const requestException: AxiosError = error;
				if (requestException.response?.status && requestException.response?.status !== 401)
					Swal.fire({
						icon: 'error',
						title: 'Error',
						text: `Ocurrio un error inesperado con codigo ${requestException.response?.status}, trate nuevamente`,
						timerProgressBar: true,
						timer: 2500,
					});
				else if (requestException.response?.status === 401)
					Swal.fire({
						icon: 'warning',
						title: 'Datos incorrectos',
						text: `El usuario o contranseña no coincide con nuestros registros, reviselos y trate nuevamente`,
						timerProgressBar: true,
						timer: 2500,
					});
				else console.error(error);
			}
		},
		syncUser: async function({ commit }) {
			try {
				const { data } = await Axios.get('api/currentUser');
				if (data.success) {
					commit('setAuthUser', data);
					commit('setValidity', data);
					commit('setAuthState', true);
				}
			} catch (error) {
				console.error(error);
			}
		},
		signOut: async function({ commit }, ask: boolean) {
			try {
				if (ask) {
					const result = await Swal.fire({
						icon: 'question',
						title: 'Cerrar Sesion',
						text: 'Desea cerrar sesion?',
						showDenyButton: true,
						showCancelButton: false,
						confirmButtonText: `Si`,
						denyButtonText: `No`,
						confirmButtonColor: '#dc3545',
						denyButtonColor: '#007bff',
					});
					if (result.isConfirmed) {
						const { data } = await Axios.get('api/logout');
						if (data.success) {
							commit('wipeAuth');
							wipeCookies();
						}
					}
				} else {
					const { data } = await Axios.get('api/logout');
					if (data.success) {
						commit('wipeAuth');
						wipeCookies();
					}
				}
			} catch (error) {
				console.error(error);
			}
		},
	},
});
