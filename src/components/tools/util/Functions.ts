/**
 * Validates the Vee validate props to provide input valid state
 *
 * 	@param errors Error array from the provider
 * 	@param valid Flag from the vee validate provider
 * 	@returns Current state of the provider validation
 */
export function isValid(errors: string[], valid: boolean | undefined): boolean | null {
	return errors[0] ? false : valid ? true : null;
}

import Cookies from 'js-cookie';
/**
 * Removes all cookies from the current session
 *
 * @param config Cookie configuration
 */
export function wipeCookies(config?: Cookies.CookieAttributes) {
	Object.keys(Cookies.get()).forEach(cookie => {
		Cookies.remove(cookie, config);
		console.info(`Wiped Cookie [${cookie}]`);
	});
}
