import Axios, { AxiosRequestConfig } from 'axios';
import Store from '@/store';

const instanceConfig: AxiosRequestConfig = {
	withCredentials: true,
	baseURL: process.env.VUE_APP_API_BASEURL || 'http://localhost:8000/',
	headers: { 'X-Requested-With': 'XMLHttpRequest' },
};

const AxiosConfigured = Axios.create(instanceConfig);
const AxiosConfiguredInterceptors = Axios.create(instanceConfig);

const requestInterceptor = AxiosConfiguredInterceptors.interceptors.request.use(
	config => {
		Store.commit('setLoading', true);
		return config;
	},
	error => {
		if (Store.state.isLoading) Store.commit('setLoading', false);
		return Promise.reject(error);
	}
);

const responseInterceptor = AxiosConfiguredInterceptors.interceptors.response.use(
	response => {
		if (Store.state.isLoading) Store.commit('setLoading', false);
		return response;
	},
	error => {
		Store.commit('setLoading', false);
		return Promise.reject(error);
	}
);

export default AxiosConfiguredInterceptors;
export { AxiosConfigured, AxiosConfiguredInterceptors, Axios, requestInterceptor, responseInterceptor };
