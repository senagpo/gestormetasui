import Vue from 'vue';
import App from '@/App.vue';
import router from '@/router';
import store from '@/store';

import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);

import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(fas, fab, far);
Vue.component('fa-icon', FontAwesomeIcon);

import { extend, localize, setInteractionMode } from 'vee-validate';
// eslint-disable-next-line @typescript-eslint/camelcase
import { required, integer, min, max, min_value, max_value, mimes, ext, alpha_num, confirmed } from 'vee-validate/dist/rules';
import es from 'vee-validate/dist/locale/es.json';

localize('es', es);

setInteractionMode('eager');

extend('required', required);
extend('integer', integer);
extend('min', min);
extend('max', max);
extend('min_value', min_value);
extend('max_value', max_value);
extend('mimes', mimes);
extend('ext', ext);
extend('alpha_num', alpha_num);
extend('confirmed', confirmed);

import '@/assets/scss/app.scss';

Vue.config.productionTip = false;

new Vue({
	router,
	store,
	render: h => h(App),
}).$mount('#app');
